
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TriangleFrame{
    public static void main(String[] args) {
        JFrame frame=new JFrame();
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblHeight=new JLabel("Height:",JLabel.TRAILING);
        lblHeight.setSize(45, 20);
        lblHeight.setLocation(5, 5);
        lblHeight.setBackground(Color.GREEN);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);
        
        JTextField txtHeight=new JTextField();
        txtHeight.setSize(70,20);
        txtHeight.setLocation(50, 5);
        frame.add(txtHeight);
        
        JLabel lblBase=new JLabel("Base:",JLabel.TRAILING);
        lblBase.setSize(35, 20);
        lblBase.setLocation(120, 5);
        lblBase.setBackground(Color.GREEN);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
        
        JTextField txtBase=new JTextField();
        txtBase.setSize(70,20);
        txtBase.setLocation(155, 5);
        frame.add(txtBase);
        
        JButton btnCalculate=new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(225, 5);
        frame.add(btnCalculate);
        
        JLabel lblResult = new JLabel("Triangle Height= ??? Base= ??? Opposite= ??? Area=??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 250);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String strHeight = txtHeight.getText();
                    String strBase = txtBase.getText();
                    double Height = Double.parseDouble(strHeight);
                    double Base = Double.parseDouble(strBase);
                    Triangle triangle = new Triangle(Height,Base);
                    lblResult.setText("Circle Height= " + String.format("%.2f", triangle.getHeight())
                            +"Base = "+String.format("%.2f", triangle.getBase())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeight.setText("");
                    txtBase.setText("");
                    txtHeight.requestFocus();
                    
                }
                
            }
            
        });
        
        frame.setVisible(true);
    }
}
