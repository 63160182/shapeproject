
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Windows10
 */
public class SquareFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblSide = new JLabel("Side:", JLabel.TRAILING);
        lblSide.setSize(35, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.GREEN);
        lblSide.setOpaque(true);
        frame.add(lblSide);

        JTextField txtside = new JTextField();
        txtside.setSize(70, 20);
        txtside.setLocation(50, 5);
        frame.add(txtside);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(125, 5);
        frame.add(btnCalculate);

        JLabel lblResult = new JLabel("Square Side= ??? Area=??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 250);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String strSide = txtside.getText();
                    double side = Double.parseDouble(strSide);
                    Square square = new Square(side);
                    lblResult.setText("Square Height= " + String.format("%.2f", square.getSide())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtside.setText("");
                    txtside.requestFocus();

                }

            }

        });

        frame.setVisible(true);
    }
}
