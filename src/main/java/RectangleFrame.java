
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class RectangleFrame {
    public static void main(String[] args) {
        JFrame frame=new JFrame();
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWidth=new JLabel("Width:",JLabel.TRAILING);
        lblWidth.setSize(45, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.GREEN);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);
        
        JTextField txtWidth=new JTextField();
        txtWidth.setSize(50,20);
        txtWidth.setLocation(50, 5);
        frame.add(txtWidth);
        
        JLabel lblHeight=new JLabel("Height:",JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(95, 5);
        lblHeight.setBackground(Color.GREEN);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);
        
        
        JTextField txtHeight=new JTextField();
        txtHeight.setSize(50,20);
        txtHeight.setLocation(155, 5);
        frame.add(txtHeight);
        
        JButton btnCalculate=new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(225, 5);
        frame.add(btnCalculate);
        
        JLabel lblResult = new JLabel("Rectangle Width= ??? Height= ??? Area=??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 250);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.pink);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHeight = txtHeight.getText();
                    double Width = Double.parseDouble(strWidth);
                    double Height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(Width,Height);
                    lblResult.setText("Rectangle Width= " + String.format("%.2f", rectangle.getWidth())
                            +"Base = "+String.format("%.2f", rectangle.getHeight())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtHeight.setText("");
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    
                }
                
            }
            
        });
        
        frame.setVisible(true);
    }
}
