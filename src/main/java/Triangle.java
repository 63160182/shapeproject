
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.Math;
import static java.lang.Math.sqrt;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Windows10
 */
public class Triangle extends Shape {
    private double height;
    private double base;
    private double opposite;
    
    public Triangle(double height,double base){
        super("Triangle");
        this.height=height;
        this.base=base;
        this.opposite=(Math.sqrt((Math.pow(height, 2))+(Math.pow(base, 2))));
    }

    @Override
    public double calArea() {
        return 1/2.0*height*base;
    }

    @Override
    public double calPerimeter() {
        return opposite+height+base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getOpposite() {
        return opposite;
    }

    public void setOpposite(double opposite) {
        this.opposite = opposite;
    }

}